import praw
import time
import datetime
import nltk
from IPython import embed

def check(comment_list):
	for comment in comment_list:
		if type(comment) is praw.objects.MoreComments:
			return True

	return False

def utc_to_datetime(utc):
	dt = datetime.datetime.fromtimestamp(utc)

	return dt

def date_to_utc(year, month, day):
	d = datetime.date(year, month, day)
	
	return time.mktime(d.timetuple())

def flatten(list_of_lists):
	flattened = []
	for sublist in list_of_lists:
		if hasattr(sublist, '__len__'):
		    for val in sublist:
		        flattened.append(val)
		else:
			flattened.append(sublist)

	return flattened

def get_more_comments(more_comments):
	"""Takes object of type praw.objects.MoreComments and recursively returns
	all objects of type praw.objects.Comment returned by 
	more_comments.comments()."""
	try:
		more_comments.comments()
	except:
		return []

	comments = []
	for comment in more_comments.comments():
		if type(comment) is praw.objects.MoreComments:
			comments += get_more_comments(comment)
		else:
			comments += [comment]

	return comments

def get_child_comments(comment):
	"""Takes object of type praw.objects.Comments and recursively returns
	all objects of type praw.objects.Comment stored within comment.replies."""
	
	if type(comment) is praw.objects.MoreComments:
		comments = get_more_comments(comment)
		comments = [get_child_comments(comment) for comment in comments]

		return comments
	
	else:	
		children = [comment]
		if len(comment.replies) == 0:
			return children
		else:
			for child in comment.replies:
				if type(child) is praw.objects.MoreComments:
					more_comments = get_more_comments(child)
					children += [get_child_comments(comment) for comment in more_comments]
				else:
					children += [child]
					children += get_child_comments(child)

			return children

 
user_agent = 'for_science'
user_name = 'iamiamwhoami'
r = praw.Reddit(user_agent)

d1 = date_to_utc(2016, 3, 2)
d2 = date_to_utc(2016, 3, 3)

submission_generator = praw.helpers.submissions_between(r, 'AskReddit', d1, d2)
submissions = list(submission_generator)

top_level_comments = flatten([submission.comments for submission in submissions])

top_level_comments = flatten(
	[get_more_comments(comment) 
	if type(comment) is praw.objects.MoreComments 
	else comment 
	for comment in top_level_comments ])

all_comments = [get_child_comments(comment) for comment in top_level_comments]
all_comments = flatten(flatten(all_comments))

	tokens = nltk.word_tokenize(comment_text)
	tagged = nltk.pos_tag(tokens)


